package com.prototype.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

/* Activity for detecting and displaying NFC-enabled devices */
public class DevicesActivity extends Activity {

	private ListView devicesList;
	private RequestAccessDialog requestDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_devices);
    	devicesList = (ListView) findViewById(R.id.list_devices);
  	}

	@Override 
	public void onResume() { 
		super.onResume(); 
		Intent intent = this.getIntent();
		if(NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
			Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			if(rawMsgs != null) { 
				NdefMessage[] messages = new NdefMessage[rawMsgs.length]; 
				for(int i = 0; i < rawMsgs.length; i++) {
					messages[i] = (NdefMessage) rawMsgs[i]; 
				} 
				String msg = new String(messages[0].getRecords()[0].getPayload());
				if(msg.substring(3).contains("http://www.dmi.dk")) { // RFID tag's value
					String[] resources = {"First-line", "Second-line"};
					List<Map<String, String>> devices = new ArrayList<Map<String, String>>();
					Map<String, String> data = new HashMap<String, String>(2);
			        data.put("First-line", "Front-doors");
			        data.put("Second-line", "open");
			        devices.add(data);
			        SimpleAdapter adapter = new SimpleAdapter(this, devices, android.R.layout.simple_list_item_2, resources, new int[] {android.R.id.text1, android.R.id.text2});
					devicesList.setAdapter(adapter); 
					requestDialog = new RequestAccessDialog(); 
					devicesList.setOnItemClickListener(new OnItemClickListener() { 
						public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) { 
							requestDialog.show(getFragmentManager(), "requestDialog"); 
						} 
					}); 
				} 
			} 
		} 
	}

	/* Dialog for entering passkey */
	public class RequestAccessDialog extends DialogFragment {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			LayoutInflater inflater = getActivity().getLayoutInflater();
			builder.setView(inflater.inflate(R.layout.dialog_request, null));
			builder.setPositiveButton("Access",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							Dialog d = (Dialog) dialog;
							EditText pass = (EditText) d
									.findViewById(R.id.dialog_request_passkey);
							if (!pass.getText().toString().isEmpty()) {
								Intent intent = new Intent(getBaseContext(),
										DeviceActivity.class);
								intent.putExtra("passkey", pass.getText()
										.toString());
								startActivity(intent);
							} else {
								Toast toast = Toast.makeText(
										getApplicationContext(),
										"Passkey is missing! Cancelling...",
										Toast.LENGTH_SHORT);
								toast.show();
							}
						}
					}).setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
						}
					});
			return builder.create();
		}
	}
}
