package com.prototype.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import android.widget.Toast;

public class MainActivity extends Activity {
	
	private Button devices;
	private Button tokens;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
            
        devices = (Button) findViewById(R.id.button_devices);
        devices.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
				Intent intent = new Intent(getBaseContext(), DevicesActivity.class);
				startActivity(intent);
			}
        });
        
        tokens = (Button) findViewById(R.id.button_tokens);
        tokens.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(getBaseContext(), TokensActivity.class);
				startActivity(intent);
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);    
        return true;
    }
     
    @Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
    	Toast toast = Toast.makeText(getApplicationContext(), "Not implemented yet!", Toast.LENGTH_SHORT);
    	toast.show();
		return super.onMenuItemSelected(featureId, item);
	}
}