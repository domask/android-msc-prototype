package com.prototype.client;

import android.app.Application;

/* Storage for application wide variables */
public class Storage extends Application {
	
	private String host_home = "http://192.168.1.100:9000/";
	private String host_issuer = "http://192.168.1.101:9000/";
	
	public String getHostHome() {
		return host_home;
	}
	
	public void setHostHome(String h) {
		host_home = h;
	}
	
	public String getHostIssuer() {
		return host_issuer;
	}
	
}