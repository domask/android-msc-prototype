package com.prototype.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.microsoft.uprove.IssuerKeyAndParameters;
import com.microsoft.uprove.IssuerParameters;
import com.microsoft.uprove.IssuerSetupParameters;
import com.microsoft.uprove.PresentationProof;
import com.microsoft.uprove.PresentationProtocol;
import com.microsoft.uprove.Prover;
import com.microsoft.uprove.ProverProtocolParameters;
import com.microsoft.uprove.UProveKeyAndToken;
import com.microsoft.uprove.UProveToken;
import com.prototype.client.DeviceActivity.MyWriter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class TokensActivity extends Activity {
	
	private ProgressDialog progressDialog;
	private JSONObject tokens;
	private AvailableTokensDialog availableTokensDialog;
	private ListView list;
	private ListView list_tokens;
	private List<String> names;
	private List<String> token_names;
	private Map<String, String> name_id;
	private Storage storage;
	private TokenData tokendata;
	private Map<String, Prover> provers;
	private Map<String, IssuerParameters> ips;
	private XStream xs;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tokens);
		storage = ((Storage)getApplication());
		tokendata = new TokenData(getBaseContext());
		list_tokens = (ListView) findViewById(R.id.list_tokens);
		token_names = new ArrayList<String>();
		token_names = loadTokenNamesFromDb();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, token_names);
		list_tokens.setAdapter(adapter);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.activity_tokens, menu);
	    return true;
	}
	
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch(item.getItemId()) {
			case R.id.menu_token_add:
				progressDialog = ProgressDialog.show(this, "Please wait", "Loading available tokens...");
				String URL = storage.getHostIssuer() + "issuer/tokens";
				new LoadAvailableTokens().execute(URL);
				return true;
			case R.id.menu_token_delete:
				Toast toast2 = Toast.makeText(getApplicationContext(), "delete!", Toast.LENGTH_SHORT);
				toast2.show();
				return true;
		}
		return super.onMenuItemSelected(featureId, item);
	}
	
	
	/* Dialog for displaying available tokens */
	public class AvailableTokensDialog extends DialogFragment {
		
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View dialogLayout = inflater.inflate(R.layout.dialog_token, null);
			names = new ArrayList<String>();
			name_id = new HashMap<String, String>();
			ips = new HashMap<String, IssuerParameters>();
			provers = new HashMap<String, Prover>();
			try {
				JSONArray token_names = tokens.getJSONArray("tokens");
				int size = token_names.length();
				for(int i = 0; i < size; i++) {
					JSONObject element = token_names.getJSONObject(i);
					name_id.put(element.getString("name"), element.getString("id"));
					names.add(element.getString("name"));
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
			list = (ListView) dialogLayout.findViewById(R.id.list_available_tokens);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_multiple_choice, names);
			list.setAdapter(adapter);
			list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			list.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				}
			});
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setView(dialogLayout);
			return builder.create();
		}
	}

	/* Load available tokens from the issuer */
	class LoadAvailableTokens extends AsyncHttpGet {
		
		@Override 
		protected void onPostExecute(String result) {
			progressDialog.dismiss();
			try {
				tokens = new JSONObject(result);
				if(!tokens.isNull("tokens")) {
					availableTokensDialog = new AvailableTokensDialog();
					availableTokensDialog.show(getFragmentManager(), "AvailableTokensDialog");
				} else {
					// TODO: notify that there are no tokens
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}			
		}
		
	}
	
	/* First step of IssuanceProtocol in U-Prove */ 
	class StartIssuanceProtocol extends AsyncHttpGet {
		
		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();
			try {
				Document xml = XMLfromString(result);
				NodeList name_nodes = xml.getElementsByTagName("name");
				String name = name_nodes.item(0).getTextContent();
				
				NodeList size_nodes = xml.getElementsByTagName("size");
				String size = size_nodes.item(0).getTextContent();
				
				NodeList message_nodes = xml.getElementsByTagName("message");
				Node message_node = message_nodes.item(0); 
				NodeList message_arrays = message_node.getChildNodes();
				Node message_array = message_arrays.item(0);
				NodeList messages = message_array.getChildNodes();
				int msg_length = messages.getLength();
				byte[][] message1 = new byte[msg_length][];
				for(int i = 0; i < msg_length; i++) {
					String msg = messages.item(i).getTextContent();
					byte[] decoded_msg = Base64.decode(msg, Base64.DEFAULT);
					message1[i] = decoded_msg;		
				}
				NodeList attributes_nodes = xml.getElementsByTagName("attributesfull");
				Node attributes_node = attributes_nodes.item(0); 
				NodeList attributes_arrays = attributes_node.getChildNodes();
				Node attributes_array = attributes_arrays.item(0);
				NodeList attributes = attributes_array.getChildNodes();
				int a_length = attributes.getLength();
				byte[][] a = new byte[a_length][];
				for(int i = 0; i < a_length; i++) {
					String attr = attributes.item(i).getTextContent();
					byte[] decoded_attr = Base64.decode(attr, Base64.DEFAULT);
					a[i] = decoded_attr;
				}
				NodeList public_nodes = xml.getElementsByTagName("publickey");
				Node public_node = public_nodes.item(0);
				NodeList public_arrays = public_node.getChildNodes();
				Node public_array = public_arrays.item(0); // array-array
				NodeList public_keys = public_array.getChildNodes();
				int public_length = public_keys.getLength();
				byte[][] public_key = new byte[public_length][];
				for(int i = 0; i < public_length; i++) {
					String pk = public_keys.item(i).getTextContent();
					byte[] decoded_pk = Base64.decode(pk, Base64.DEFAULT);
					public_key[i] = decoded_pk;
				}
				
				NodeList prover_nodes = xml.getElementsByTagName("proverissuancevalues");
				Node prover_node = prover_nodes.item(0);
				NodeList prover_arrays = prover_node.getChildNodes();
				Node prover_array = prover_arrays.item(0);
				NodeList prover_values = prover_array.getChildNodes();
				int prover_length = prover_values.getLength();
				byte[][] prover_issuance_values = new byte[prover_length][];
				for(int i = 0; i < prover_length; i++) {
					String piv = prover_values.item(i).getTextContent();
					byte[] decoded_piv = Base64.decode(piv, Base64.DEFAULT);
					prover_issuance_values[i] = decoded_piv;
				}			
				byte[] s = new byte[Integer.parseInt(size)];
				IssuerSetupParameters isp = new IssuerSetupParameters();
				isp.setEncodingBytes(s);
				isp.setHashAlgorithmUID("SHA-256");
				isp.setParametersUID("unique UID".getBytes());
				IssuerKeyAndParameters ikap = isp.generate();
				IssuerParameters ip = ikap.getIssuerParameters();
				ip.setPublicKey(public_key);
				ip.setProverIssuanceValues(prover_issuance_values);
				ip.validate();
				xs = new XStream(new DomDriver() {
							public HierarchicalStreamWriter createWriter(Writer out) {
									return new MyWriter(out);
							}
						});			
				byte[] tokenInformation = "token information".getBytes();
				byte[] proverInformation = "prover information".getBytes();
				ProverProtocolParameters proverProtocolParams = new ProverProtocolParameters();
				proverProtocolParams.setIssuerParameters(ip);
				proverProtocolParams.setNumberOfTokens(1);
				proverProtocolParams.setTokenAttributes(a);
				proverProtocolParams.setTokenInformation(tokenInformation);
				proverProtocolParams.setProverInformation(proverInformation);
				Prover prover = proverProtocolParams.generate();	
				String prover_id  = "prover_" + Integer.toString(prover.hashCode());
				String issuer_id  = "issuer_" + Integer.toString(ip.hashCode());
				provers.put(prover_id, prover);
				ips.put(issuer_id, ip);
				byte[][] message2 = prover.generateSecondMessage(message1);
				String temp = xs.toXML(message2);
				temp = temp.replaceAll("[\\n\\t ]", "");
				String msg2 = "<message>" + temp + "</message>";
				String attribs = xs.toXML(a);
				new FinishIssuanceProtocol().execute(super.getUrl(), msg2, prover_id, issuer_id, name, size, attribs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
 		
	public void downloadSelected(View view) {
		if(view.getId() == R.id.button_collect) {
			int size = list.getCheckedItemCount();
			if(size > 0) {
				SparseBooleanArray checked = list.getCheckedItemPositions();
				progressDialog = ProgressDialog.show(this, "Please wait", "Loading available tokens...");
				for(int i = 0; i < checked.size(); i++) {
					if(checked.valueAt(i) == true) {         
						String URL = storage.getHostIssuer() + "issuer/token/" + name_id.get(names.get(checked.keyAt(i)));		
						Log.d("URL", URL);
						new StartIssuanceProtocol().execute(URL);
					}
				}
			} else {
				Toast toast = Toast.makeText(getApplicationContext(), "Please select at least one token!", Toast.LENGTH_SHORT);
				toast.show();				
			}
		}
	}
		
	public void cancelDialog(View view) {
		if(view.getId() == R.id.button_cancel) {
			availableTokensDialog.dismiss();
		}
	}
	
	public List<String> loadTokenNamesFromDb() {
		List<String> list = new ArrayList<String>();
		Cursor cursor = tokendata.all();
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			int index = cursor.getColumnIndex("name");
		    list.add(cursor.getString(index));
		    cursor.moveToNext();
		}
		cursor.close();
		return list;
	}
	
	/* Third step of IssuanceProtocol */
	class FinishIssuanceProtocol extends AsyncTask<String, String, SixSixSix> {
		@Override
		protected SixSixSix doInBackground(String... params) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(params[0]);
			HttpResponse response;	
			String responseString = null;
			StringEntity se = null;
			try {
				se = new StringEntity(params[1]);
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
	        httppost.setEntity(se);
			try {
				response = httpclient.execute(httppost);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				responseString = "Cannot connect to the device!";
			}
			SixSixSix six = new SixSixSix();
			six.response = responseString;
			six.proverId = params[2];
			six.ipId = params[3];
			six.name = params[4];
			six.size = params[5];
			six.attributes = params[6];
			return six;
		}
		
		@Override
		protected void onPostExecute(SixSixSix six) {
			try {
				byte[][] message3 = (byte[][]) xs.fromXML(six.response);
				byte[][] attributes = (byte[][]) xs.fromXML(six.attributes);
				UProveKeyAndToken[] upkt = provers.get(six.proverId).generateTokens(message3);
				UProveKeyAndToken keyAndToken = upkt[0];
				UProveToken t = keyAndToken.getToken();
				int s = Integer.parseInt(six.size);
				int[] d = new int[s];
				for(int i = 0; i < s; i++) {
					d[i] = i+1;
				}
				/* Store data locally
				 * TODO: Implement presentationProtocol according to U-Prove specifications
				 */
				PresentationProof p = PresentationProtocol.generatePresentationProof(ips.get(six.ipId), d, null, null, keyAndToken, attributes);
				if(!tokendata.exists(six.name)) {
					String token = xs.toXML(t);
					String proof = xs.toXML(p);
					String params = xs.toXML(ips.get(six.ipId));
					String disclosed = xs.toXML(d);
					tokendata.insert(six.name, token, proof, disclosed, params);
				} else {
					Log.d("TOKEN EXISTS", six.name);
				}
				Log.e("Issuance Protocol", six.name);
				availableTokensDialog.dismiss();
				token_names = loadTokenNamesFromDb();
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, token_names);
				adapter.notifyDataSetChanged();
				list_tokens.setAdapter(adapter);
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public Document XMLfromString(String v){
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(v));
            doc = db.parse(is); 
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            System.out.println("Wrong XML file structure: " + e.getMessage());
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }	
	
	public class SixSixSix {
	    public String response;
	    public String proverId;
	    public String ipId;
	    public String size;
	    public String name;
	    public String attributes;
	}
}
