package com.prototype.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.thoughtworks.xstream.core.util.QuickWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;

/* Activity for communicating with access control system */
public class DeviceActivity extends Activity {
	
	private ProgressDialog progressDialog;
	private TextView textview;
	private List<String> attributes;
	private ListView attributesList;
	private Map<String, String> tokens;
	private List<String> token_names;
	private ListView tokenList;
	private Storage storage;
	private TokenData tokendata;
	private AvailableTokensDialog availableTokensDialog;
	private String sessionId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_device);
		Bundle bundle = getIntent().getExtras();
		String passkey = bundle.getString("passkey");
		progressDialog = ProgressDialog.show(this, "Please wait", "Requesting an access...");
		textview = (TextView) findViewById(R.id.device_response);	
		attributesList = (ListView) findViewById(R.id.list_attributes);
		tokendata = new TokenData(getBaseContext());
		storage = ((Storage)getApplicationContext());
		String URL = storage.getHostHome() + "pep";
		new RequestTask().execute(URL, passkey);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}
	
	class RequestTask extends AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... params) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(params[0]);
			HttpResponse response;
			String responseString = null;
			try {
		        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		        nameValuePairs.add(new BasicNameValuePair("passkey", params[1]));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				response = httpclient.execute(httppost);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				responseString = "Cannot connect to the device!";
			}
			return responseString;
		}
		
		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();
			try {
				Document response = loadXMLFromString(result);
				XPath xpath = XPathFactory.newInstance().newXPath();
				XPathExpression expr = xpath.compile("//Decision/text()");
				Object value = expr.evaluate(response, XPathConstants.STRING);	
				if(value.toString().equals(new String("Permit"))) {
					sessionId = response.getElementsByTagName("SessionId").item(0).getTextContent();
					NodeList obligations = response.getElementsByTagName("AttributeAssignment");
					Integer length = obligations.getLength();
					if(length > 0) {
						attributes = new ArrayList<String>();
						for(int i = 0; i < length; i++) {
							Node attribute = obligations.item(i);
							attributes.add(attribute.getTextContent());
						}
					showAttributes(attributes);
					} else {
						setContentView(R.layout.fragment_permit);
					}
				} else {
					setContentView(R.layout.fragment_deny);
				}
			} catch (Exception e) {
				textview.setText("Problems parsing XML file!");
			}
		}
	}
	
	public Document loadXMLFromString(String xml) throws Exception {
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    return builder.parse(new ByteArrayInputStream(xml.getBytes()));
	}
	
	// Fragment's onClick event
    public void backToDevices(View view) {
    	if(view.getId() == R.id.button_try_again) {
    		this.finish();
    	}
    }
    
	public void showAttributes(List<String> attributes) {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, attributes);
		adapter.notifyDataSetChanged();
		attributesList.setAdapter(adapter);
	}
	
	// Send Attributes button's onClick event
	public void chooseToken(View view) {
		if(view.getId() == R.id.button_choose_token) {
			availableTokensDialog = new AvailableTokensDialog();
			availableTokensDialog.show(getFragmentManager(), "AvailableTokensDialog");
		}
	}
	
	public void cancelAttributes(View view) {
		if(view.getId() == R.id.button_cancel) {
			this.finish();
		}
	}
	
	/* Dialog for showing available/stored tokens */
	public class AvailableTokensDialog extends DialogFragment {
		
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View dialogLayout = inflater.inflate(R.layout.dialog_choose_token, null);
			tokenList = (ListView) dialogLayout.findViewById(R.id.list_tokens);
			tokens = loadTokenNamesFromDb();
			token_names = new ArrayList<String>(tokens.keySet());
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, token_names);
			tokenList.setAdapter(adapter);
			tokenList.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					String id = tokens.get(token_names.get(arg2));
					String URL = storage.getHostHome() + "pep/token";
					String xml = generateTokenInfo(id);
					new SendToken().execute(URL, xml);
				}
			});
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setView(dialogLayout);
			return builder.create();
		}
	}
	
	/* Sending token to the access control system */
	class SendToken extends AsyncTask<String, String, String> {
		@Override
		protected String doInBackground(String... params) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(params[0]);
			HttpResponse response;
			String responseString = null;
			StringEntity se = null;
			try {
				se = new StringEntity(params[1]);
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
	        httppost.setEntity(se);
			try {
				response = httpclient.execute(httppost);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				responseString = "Cannot connect to the device!";
			}
			return responseString;
		}
		
		@Override
		protected void onPostExecute(String result) {
			availableTokensDialog.dismiss();
			try {
				if(result.equals(new String("Permit"))) {
					setContentView(R.layout.fragment_permit);
				} else {
					setContentView(R.layout.fragment_deny);
					TextView msg = (TextView) findViewById(R.id.textview_acess_deny_msg);
					msg.setText(result);
				}
			} catch (Exception e) {
				e.printStackTrace();			
			}
		}
	}
	
	public String generateTokenInfo(String id) {
		String xml = "";
		Cursor cursor = tokendata.find(id);
		cursor.moveToFirst();
		try {
			xml = "<data>";
			xml += "<sessionId>" + sessionId + "</sessionId>";
			xml += "<disclosed>" + cursor.getString(cursor.getColumnIndex("disclosed")) + "</disclosed>";
			xml += "<token>" + cursor.getString(cursor.getColumnIndex("token")) + "</token>";
			xml += "<proof>" + cursor.getString(cursor.getColumnIndex("proof")) + "</proof>";
			xml += "<params>" + cursor.getString(cursor.getColumnIndex("params")) + "</params>";
			xml += "</data>";
		} catch(Exception e) {
			e.printStackTrace();
		}
		cursor.close();
		return xml;
	}
	
	public Map<String, String> loadTokenNamesFromDb() {
		Map<String, String> map = new HashMap<String, String>();
		Cursor cursor = tokendata.all();
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			int name = cursor.getColumnIndex("name");
			int id	= cursor.getColumnIndex("_id");
			map.put(cursor.getString(name), cursor.getString(id));
		    cursor.moveToNext();
		}
		return map;
	}
	
	public static class MyWriter extends PrettyPrintWriter {
	    public MyWriter(Writer writer) {
	        super(writer);
	    }

	    protected void writeText(QuickWriter writer, String text) { 
	        if (text.indexOf('<') < 0) {
	            writer.write(text);
	        } else { 
	            writer.write(text); 
	        }
	    }
	}
	
}