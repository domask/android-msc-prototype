package com.prototype.client;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class TokenData extends SQLiteOpenHelper {
	
	  private static final String DATABASE_NAME = "uprove_prototype.db";
	  private static final int DATABASE_VERSION = 33;

	  public static final String TABLE_NAME = "tokens";

	  public static final String _ID = BaseColumns._ID;
	  public static final String NAME = "name";
	  public static final String TOKEN = "token";
	  public static final String PROOF = "proof";
	  public static final String DISCLOSED = "disclosed";
	  public static final String PARAMS = "params";

	  public TokenData(Context context) {
		  super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }
	  
	  @Override
	  public void onCreate(SQLiteDatabase db) {
		  String sql = "CREATE TABLE " + TABLE_NAME + " ("
				+ _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ NAME + " TEXT NOT NULL, "
				+ TOKEN + " TEXT NOT NULL, "
				+ PROOF + " TEXT NOT NULL," 
				+ DISCLOSED + " TEXT NOT NULL,"
				+ PARAMS + " TEXT NOT NULL);";
		  db.execSQL(sql);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		  db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		  onCreate(db);
	  }
	  
	  public void insert(String name, String token, String proof, String disclosed, String params) {
		  SQLiteDatabase db = getWritableDatabase();
		  ContentValues values = new ContentValues();
		  values.put(NAME, name);
		  values.put(TOKEN, token);
		  values.put(PROOF, proof);
		  values.put(DISCLOSED, disclosed);
		  values.put(PARAMS, params);
		  db.insertOrThrow(TABLE_NAME, null, values);
		  Log.d("storing", name);
	  }
	  
	  public Cursor find(String id) {
		  SQLiteDatabase db = getReadableDatabase();
		  String query = "SELECT " + NAME + ", " + TOKEN + ", " + PROOF + ", " + DISCLOSED + ", " + PARAMS  + " FROM " + TABLE_NAME + " WHERE " + _ID + " = " + id;
		  Cursor cursor = db.rawQuery(query, null);
		  return cursor;
	  }
	  
	  public boolean exists(String name) {
		  SQLiteDatabase db = getReadableDatabase();
		  String query = "SELECT " + NAME + " FROM " + TABLE_NAME + " WHERE " + NAME + " = '"+name+"'";
		  Cursor cursor = db.rawQuery(query, null);
		  boolean exists = (cursor.getCount() > 0);
		  cursor.close();
		  return exists;
	  }
	  
	  public Cursor all() {
		  String[] from = { _ID, NAME, TOKEN, PROOF, DISCLOSED, PARAMS };
		  String order = NAME;
		  
		  SQLiteDatabase db = getReadableDatabase();
		  Cursor cursor = db.query(TABLE_NAME, from, null, null, null, null, order);
		  return cursor;
	  }
}