package com.prototype.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;

// TODO: implement AsyncHttpPost
class AsyncHttpGet extends AsyncTask<String, String, String> {
	
	private String url;
	
	@Override
	protected String doInBackground(String... params) {
		
		HttpClient httpclient = new DefaultHttpClient();
		HttpResponse response;
		String responseString = null;
		if(!params[0].isEmpty() && params[0] != null) {
			url = params[0];
			HttpGet httpget = new HttpGet(params[0]);
			try {
				response = httpclient.execute(httpget);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					response.getEntity().writeTo(out);
					out.close();
					responseString = out.toString();
				} else {
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				responseString = "Cannot connect to the device!";
			}
			return responseString;
		} else {
			throw new IllegalStateException("URL not found!");
		}
		
	}
	
	public String getUrl() {
		return url;
	}
}
